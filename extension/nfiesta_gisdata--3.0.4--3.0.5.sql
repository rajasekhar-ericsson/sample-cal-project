UPDATE @extschema@.c_config_query set
label = 'Součet existujících veličin',
label_en = 'Sum of existing variables'
WHERE id = 200;

UPDATE @extschema@.c_config_query set
label = 'Doplněk do úhrnu existující veličiny',
label_en = 'Addition to the total of the existing variable'
WHERE id = 400;

UPDATE @extschema@.c_config_query set
label = 'Odkaz na existující veličinu',
label_en = 'A link to the existing variable'
WHERE id = 500;

UPDATE @extschema@.c_ext_version set
description = 'Verze 3.0.1 - extenze nfiesta_gisdata pro pomocná data. Nové pohledy export_api u výpočetních buněk.',
label_en = '3.0.1',
description_en = 'Version 3.0.1 - nfiesta_gisdata extension for auxiliary data. New export_api views of estimation cells.'
WHERE id = 1300;

UPDATE @extschema@.c_ext_version set
description = 'Verze 3.0.2 - extenze nfiesta_gisdata pro pomocná data. Přidání sloupce estimation_cell_collection do pohledu estimation_cell.',
label_en = '3.0.2',
description_en = 'Version 3.0.2 - nfiesta_gisdata extension for auxiliary data. Addition of estimation_cell_collection column into estimation_cell view.'
WHERE id = 1400;

UPDATE @extschema@.c_ext_version set
description = 'Verze 3.0.3 - extenze nfiesta_gisdata pro pomocná data. Pohled estimation_cell_hierarchy mění identifikátory výpočetních buněk z id na labely.',
label_en = '3.0.3',
description_en = 'Version 3.0.3 - nfiesta_gisdata extension for auxiliary data. The estimation_cell_hierarchy view changes estimation cell identifiers from ids to labels.'
WHERE id = 1500;

UPDATE @extschema@.c_ext_version set
description = 'Verze 3.0.4 - extenze nfiesta_gisdata pro pomocná data. Opraven changescript 3.0.3, do kterého se zapomněl přidat .sql pohledu.',
label_en = '3.0.4',
description_en = 'Version 3.0.4 - nfiesta_gisdata extension for auxiliary data. Corrected changescript 3.0.3, where the .sql of the view was forgotten to add.'
WHERE id = 1600;

INSERT INTO @extschema@.c_ext_version (id, label, description, label_en, description_en)
VALUES (1700, '3.0.5', 'Verze 3.0.5 - extenze nfiesta_gisdata pro pomocná data. Nahrazení některých pojmů v tab. c_config_query.', '3.0.5', 'Version 3.0.5 - nfiesta_gisdata extension for auxiliary data. Replacing some terms in the c_config_query table.');

UPDATE @extschema@.cm_ext_config_function set
ext_version_valid_until = '1700'
WHERE id = 5;

UPDATE @extschema@.cm_ext_config_function set
ext_version_valid_until = '1700'
WHERE id = 6;

UPDATE @extschema@.cm_ext_config_function set
ext_version_valid_until = '1700'
WHERE id = 7;

UPDATE @extschema@.cm_ext_config_function set
ext_version_valid_until = '1700'
WHERE id = 8;
 








