--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
/*
	multithreaded calculation of functions for point intersection => Issue 8 on GitLab
*/

---------------------------------------------------------------------------------------------------;
-- add ext_version 2.0.0
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.c_ext_version(id, label, description)
VALUES
	(1000,'2.0.0','Version 2.0.0 - extension nfiesta_gisdata for auxiliary data. Extension of point intersection functions by multi-threaded calculation for GUI application.');
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- t_auxiliary_data
---------------------------------------------------------------------------------------------------;
ALTER TABLE @extschema@.t_auxiliary_data ADD COLUMN gid integer;
COMMENT ON COLUMN @extschema@.t_auxiliary_data.gid IS 'Foreign key on column GID into table f_p_plot.';

ALTER TABLE @extschema@.t_auxiliary_data
  ADD CONSTRAINT fkey__t_auxiliary_data__f_p_plot__gid FOREIGN KEY (gid)
      REFERENCES @extschema@.f_p_plot (gid) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE;
COMMENT ON CONSTRAINT fkey__t_auxiliary_data__f_p_plot__gid ON @extschema@.t_auxiliary_data IS 'Foreign key on column GID into table f_p_plot.';

CREATE INDEX fki__t_auxiliary_data__f_p_plot
  ON @extschema@.t_auxiliary_data
  USING btree
  (gid);
COMMENT ON INDEX @extschema@.fki__t_auxiliary_data__f_p_plot
  IS 'Index over foreign key fkey__t_auxiliary_data__f_p_plot__gid.';

ALTER TABLE @extschema@.t_auxiliary_data ADD COLUMN est_date timestamp NOT NULL DEFAULT now();
COMMENT ON COLUMN @extschema@.t_auxiliary_data.gid IS 'Date and time of calculation of the auxiliary variable.';

-- ALTER TABLE @extschema@.t_auxiliary_data DROP COLUMN ident_point;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- Functions --
---------------------------------------------------------------------------------------------------;


------------------------------------------------------------------------------;
-- <function name="fn_get_interval_points_app" schema="@extschema@" src="functions/extschema/fn_get_interval_points_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_interval_points_app(integer, integer)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_interval_points_app
(
	_config_collection	integer,
	_interval_points	integer
)
RETURNS TABLE
(
	step4interval		integer,
	gid_start		integer,
	gid_end			integer,
	interval_points		integer
)
AS
$BODY$
DECLARE
	_total_points		integer;
	_total_points_plot	integer;
	_count_intervals	integer;
BEGIN
	-----------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Error 01: fn_get_interval_points_app: The input argument _config_collection must not be NULL!';
	END IF;

	IF _interval_points IS NULL OR _interval_points < 0
	THEN
		RAISE EXCEPTION 'Error 02: fn_get_interval_points_app: The input argument _interval_points must not be NULL or a negative value!';
	END IF;
	-----------------------------------------------------------------------
	-- finding total_points for _config_collection [ref_id_layer_points] 
	SELECT total_points FROM @extschema@.t_config_collection WHERE id = _config_collection
	INTO _total_points;
	-----------------------------------------------------------------------
	IF _total_points IS NULL
	THEN
		RAISE EXCEPTION 'Error 03: fn_get_configs4aux_data_app: In configuration for point layer [ID = %] is not filled value total_points!',_config_collection;
	END IF;
	
	IF _total_points = 0
	THEN
		RAISE EXCEPTION 'Error 04: fn_get_interval_points_app: In configuration for point layer [ID = %] must not by filled value 0 in column total_points!',_config_collection;
	END IF;
	-----------------------------------------------------------------------
	-- determining the number of imported points for the point layer from
	-- the configuration 
	SELECT count(*) FROM @extschema@.f_p_plot WHERE config_collection = _config_collection
	INTO _total_points_plot;
	-----------------------------------------------------------------------
	IF _total_points_plot IS NULL OR _total_points_plot = 0
	THEN
		RAISE EXCEPTION 'Error 05: fn_get_interval_points_app: For configuration of layer of points [ID = %] are not imported data in table f_p_plot!',_config_collection;
	END IF;
	-----------------------------------------------------------------------
	IF _total_points_plot != _total_points
	THEN
		RAISE EXCEPTION 'Error 06: fn_get_interval_points_app: For the configuration of the point layer [ID = %], the number of points [%] does not match the number in the total_points field with the number of imported points [%] in the table f_p_plot!',_config_collection,_total_points,_total_points_plot;
	END IF;
	-----------------------------------------------------------------------
	IF _total_points < _interval_points
	THEN
		RAISE EXCEPTION 'Error 07: fn_get_interval_points_app: The specified number of points in the input argument _interval_points [%] is greater than the number of points [%] that the required point layer has [Configuration ID = %] for intersection!',_interval_points,_total_points,_config_collection;
	END IF;
	-----------------------------------------------------------------------
	IF _interval_points = 0
	THEN
		RETURN QUERY EXECUTE
		'
		SELECT
			1 AS step4interval,
			min(gid) AS gid_start,
			max(gid) AS gid_end,
			$1 AS interval_points
		FROM
			@extschema@.f_p_plot WHERE config_collection = $2 
		'
		USING _total_points, _config_collection;
	ELSE
		_count_intervals := ceiling((_total_points)::numeric/(_interval_points)::numeric);
	
		RETURN QUERY EXECUTE
		'
		WITH
		w1 as 	(
			SELECT
				gid,
				ROW_NUMBER () OVER (ORDER BY gid)
			FROM
				@extschema@.f_p_plot WHERE config_collection = $1
			),
		w2 as 	(
			SELECT
				i as i_interval,
				((i * $2) - $2 + 1) AS i_start,
				(i * $2) AS i_end
			FROM
				generate_series(1, $3) AS i
			),
		w3 as 	(
			SELECT
				w1.*,
				w2.*
			FROM
				w1 INNER JOIN w2
			ON
				w1.row_number >= w2.i_start
			AND
				w1.row_number <= w2.i_end
			)
		SELECT
			(i_interval)::integer AS step4interval,
			(min(gid))::integer AS gid_start,
			(max(gid))::integer AS gid_end,
			(count(gid))::integer AS interval_points
		FROM
			w3
		GROUP
			BY i_interval
		ORDER
			BY i_interval
		'
		USING _config_collection, _interval_points, _count_intervals;
	END IF;	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) IS
'The function returns an interval list for the point layer according to the set interval interval_points specifying the number of points.';
-- </function>
------------------------------------------------------------------------------;


------------------------------------------------------------------------------;
DROP FUNCTION @extschema@.fn_get_configs4aux_data_app(integer, integer, boolean);
-- <function name="fn_get_configs4aux_data_app" schema="@extschema@" src="functions/extschema/fn_get_configs4aux_data_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_configs4aux_data_app(integer, integer, integer, boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_configs4aux_data_app
(
	_config_collection	integer,
	_gui_version		integer,
	_interval_points	integer DEFAULT 0,
	_recount		boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step			integer,
	config_collection	integer,
	config			integer,
	intersects		boolean,
	step4interval		integer,
	gid_start		integer,
	gid_end			integer	
)
AS
$BODY$
DECLARE
	_ref_id_layer_points		integer;
	_ref_id_total			integer;
	_ref_id_total_categories	integer[];
	_ref_id_total_config_query	integer[];
	_config_function		integer;
	_aggregated			boolean;
	_ext_version_current		integer;
	_ext_version_current_label	text;
	_step4interval_res		integer[];
	_gid_start_res			integer[];
	_gid_end_res			integer[];
	_interval_points_res		integer[];
	_intersects			boolean;
	_config_id_reference		integer;
	_config_id_base			integer;
	_config_collection_base		integer;
	_check				integer;
	_check_exists_ii		boolean;
	_res_step_ii			integer[];
	_res_config_collection_ii	integer[];
	_res_config_ii			integer[];
	_res_intersects_ii		boolean[];
	_res_config_query_ii		integer[];
	_res_step4interval_ii		integer[];
	_res_check_exists_ii		boolean[];
	_res_step			integer[];
	_res_config_collection		integer[];
	_res_config			integer[];
	_res_intersects			boolean[];
	_res_config_query		integer[];
	_res_step4interval		integer[];
	_res_check_exists		boolean[];	
	_q				text;
	_check_1			integer;
	_check_2			integer;
	_check_3			integer;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Error 01: fn_get_configs4aux_data_app: The input argument _config_collection must not by NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Error 02: fn_get_configs4aux_data_app: The input argument _gui_version must not by NULL!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Error 03: fn_get_configs4aux_data_app: The input argument _recount must not by NULL!';
	END IF;

	IF _interval_points IS NULL OR _interval_points < 0
	THEN
		RAISE EXCEPTION 'Error 04: fn_get_configs4aux_data_app: The input argument _interval_points must not by NULL or a negative value!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- finding reference IDs for the point layer and total configuration
	SELECT
		tcc.ref_id_layer_points,
		tcc.ref_id_total
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection
	INTO
		_ref_id_layer_points,
		_ref_id_total;
	-------------------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Error 05: fn_get_configs4aux_data_app: For input argument _config_collection = % not be in configuration found a reference to point layer configuration!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total IS NULL
	THEN
		RAISE EXCEPTION 'Error 06: fn_get_configs4aux_data_app: For input argument _config_collection = % not be in configuration found a reference to configuration of total!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	-- found CATEGORY and config_query for TOTAL REFERENCE
	SELECT
		array_agg(tc.id ORDER BY tc.id) AS id,
		array_agg(tc.config_query ORDER BY tc.id) AS config_query
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.config_collection = _ref_id_total
	INTO
		_ref_id_total_categories,
		_ref_id_total_config_query;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_categories IS NULL
	THEN
		RAISE EXCEPTION 'Error 07: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena v konfiguraci [v tabulce t_config] zadna kategorie teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_config_query IS NULL
	THEN
		RAISE EXCEPTION 'Error 08: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena v konfiguraci [v tabulce t_config] zadna hodnota config_query u kategorii teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	-- detected config_function and aggregated for _ref_id_total
	SELECT
		config_function,
		aggregated
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _ref_id_total
	INTO
		_config_function,
		_aggregated;
	-------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Error 09: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena hodnota _config_function u teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _aggregated IS NULL
	THEN
		RAISE EXCEPTION 'Error 10: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena hodnota _aggregated u teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	-- find the latest version of the extension for the input version of the GUI application
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Error 11: fn_get_configs4aux_data_app: Pro vstupni argument _gui_version = % nenalezena verze extenze nfiesta_gisdata v mapovaci tabulce cm_ext_gui_version!',_gui_version;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;

	IF _ext_version_current_label IS NULL
	THEN
		RAISE EXCEPTION 'Error 12: fn_get_configs4aux_data_app: Pro argument _ext_version_current = % nenalezena verze extenze v ciselniku c_ext_version!',_ext_version_current;
	END IF;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- proces naplneni promennych pro VLAKNA
	-------------------------------------------------------------------------------------------
	-- volani funkce pro ziskani casti pro VLAKNA [v pripade, ze uzivatel nepozaduje vypocet
	-- na vice vlaken, vstupni promenna _interval_points je hodnota 0, pak funkce
	-- fn_get_interval_points_app vrati jeden radek, kde gid_start_res je jedno prvkove pole
	-- a obsahuje min(gid) z bodove vrstvy, u end je to analogicky stejne a jde pak o max]
	-- poznamka: ve funkci je kontrola, ze hodnota total_points ulozena v konfiguraci odpovida realite,
	-- pokud tomu tak je, pak v pripade, ze je _interval_points hodnota 0, funkce vrati hodnotu
	-- _interval_points rovnu hodnote _total_points pro vstupni _ref_id_layer_points
	WITH
	w AS	(
		SELECT * FROM @extschema@.fn_get_interval_points_app(_ref_id_layer_points,_interval_points)
		)
	SELECT
		array_agg(w.step4interval ORDER BY w.step4interval) AS step4interval_res,
		array_agg(w.gid_start ORDER BY w.step4interval) AS gid_start_res,
		array_agg(w.gid_end ORDER BY w.step4interval) AS gid_end_res,
		array_agg(w.interval_points ORDER BY w.step4interval) AS interval_points_res
	FROM
		w
	INTO
		_step4interval_res,
		_gid_start_res,
		_gid_end_res,
		_interval_points_res;
	-----------------------------------------
	IF _step4interval_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 13: fn_get_configs4aux_data_app: Nenaplnena interni promenna _step4interval_res!';
	END IF;
	-----------------------------------------
	IF _gid_start_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 14: fn_get_configs4aux_data_app: Nenaplnena interni promenna _gid_start_res!';
	END IF;
	-----------------------------------------
	IF _gid_end_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 15: fn_get_configs4aux_data_app: Nenaplnena interni promenna _gid_end_res!';
	END IF;
	-----------------------------------------
	IF _interval_points_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 16: fn_get_configs4aux_data_app: Nenaplnena interni promenna _interval_points_res!';
	END IF;	
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_step4interval_res) AS step4interval_res) AS t
		WHERE t.step4interval_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 17: fn_get_configs4aux_data_app: Interni promenna _step4interval_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_gid_start_res) AS gid_start_res) AS t
		WHERE t.gid_start_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 18: fn_get_configs4aux_data_app: Interni promenna _gid_start_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_gid_end_res) AS gid_end_res) AS t
		WHERE t.gid_end_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 19: fn_get_configs4aux_data_app: Interni promenna _gid_end_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_interval_points_res) AS interval_points_res) AS t
		WHERE t.interval_points_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 20: fn_get_configs4aux_data_app: Interni promenna _interval_points_res obsahuje hodnotu NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- CYKLUS po jednotlivych konfiguracnich KATEGORIICH
	FOR i IN 1..array_length(_ref_id_total_categories,1)
	LOOP
		-----------------------------------------------------------------------------------
		IF _ref_id_total_config_query[i] IS NULL
		THEN
			RAISE EXCEPTION 'Error 21: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] neni u nektere jeji kategorie vyplnena hodnota config_query!',_ref_id_total;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces naplneni promenne _intersects
		IF	(
			_config_function = ANY(array[100,200])	AND
			_ref_id_total_config_query[i] = 100 AND
			_aggregated = FALSE
			)
		THEN
			_intersects := TRUE;
		ELSE
			_intersects := FALSE;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces zjisteni _config_id_base a _config_collection_base
		IF _ref_id_total_config_query[i] = 500 -- REFERENCE
		THEN
			-- config_id_base
			SELECT tc.categories::integer
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _ref_id_total_categories[i]
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Error 22: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % [ID konfigurace uhrnu] je jeho i-ta kategorie [_ref_id_total_config_query[i] = %] config_query 500 [reference] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_ref_id_total,_ref_id_total_config_query[i];
			END IF;

			_config_id_base := _config_id_reference;

			-- config_collection_base
			SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
			WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_id_base)
			AND tcc.ref_id_layer_points = _ref_id_layer_points
			INTO _config_collection_base;
		ELSE
			_config_id_base := _ref_id_total_categories[i];
			_config_collection_base := _config_collection;
		END IF;
		-----------------------------------------------------------------------------------
		-- 1. KONTROLA EXISTENCE v DB
		-----------------------------------------------------------------------------------
		IF _recount = FALSE
		
		THEN	-- kontrola existence v db => vetev pro VYPOCET bodu

			-- CYKLUS po jednotlivych INTERVALECH
			FOR ii IN 1..array_length(_step4interval_res,1)
			LOOP
				-- => kontrola, ze dane _config_collection, dane _config a pro danou _ext_version_current
				-- JE nebo NENI v DB vypocitano [poznamka: verze u vsech bodu bude vzdy stejna !!!]
				WITH
				w1 AS	(
					SELECT tad.* FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_base
					AND tad.config = _config_id_base
					AND tad.ext_version = _ext_version_current
					AND tad.gid >= _gid_start_res[ii]
					AND tad.gid <= _gid_end_res[ii]
					)
				SELECT count(*) FROM w1
				INTO _check;

				-- varianty:
				-- 1. body pro i-ty interval uz v DB    existuji a pocet bodu se    shoduje s promennou _interval_points => tento zaznam do vypoctu NEPUJDE
				-- 2. body pro i-ty interval uz v DB    existuji a pocet bodu se ne-shoduje s promennou _interval_points => tento zaznam do vypoctu   PUJDE
				-- 3. body pro i-ty interval    v DB ne-existuji => tento zaznam do vypoctu PUJDE

				IF _check > 0
				THEN
					IF _check = _interval_points_res[ii]
					THEN
						_check_exists_ii := TRUE; -- interval nepujde do vypisu
					ELSE
						_check_exists_ii := FALSE; -- interval pujde do vypisu
					END IF;
				ELSE
					IF _ref_id_total_config_query[i] = 500	-- REFERENCE
					THEN
						RAISE EXCEPTION 'Error 23: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % [ID konfigurace uhrnu] je jeho kategorie _ref_id_total_config_query[i] = % reference, pro kterou ale neni doposud proveden vypocet kategorie z neagregovane konfigurace uhrhu!',_ref_id_total,_ref_id_total_config_query[i];
					ELSE
						_check_exists_ii := FALSE; -- interval pujde do vypisu
					END IF;
				END IF;

				-- naplneni promenych do RETURN QUERY v cyklu ii
				IF ii = 1
				THEN
					_res_step_ii := array[i];
					_res_config_collection_ii := array[_config_collection];
					_res_config_ii := array[_ref_id_total_categories[i]];
					_res_intersects_ii := array[_intersects];
					_res_config_query_ii := array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := array[ii];
					_res_check_exists_ii := array[_check_exists_ii];
				ELSE
					_res_step_ii := _res_step_ii || array[i];
					_res_config_collection_ii := _res_config_collection_ii || array[_config_collection];
					_res_config_ii := _res_config_ii || array[_ref_id_total_categories[i]];
					_res_intersects_ii := _res_intersects_ii || array[_intersects];
					_res_config_query_ii := _res_config_query_ii || array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := _res_step4interval_ii || array[ii];
					_res_check_exists_ii := _res_check_exists_ii || array[_check_exists_ii];
				END IF;				
			END LOOP;
		ELSE
			-- kontrola existence v db => vetev pro PREPOCET bodu

			-- 1. prepocet   JE MOZNY pokud v db existuji vsechny kategorie dane konfigurace [na intervalech a na verzi extenze NEZALEZI]

				-- => pocet bodu i-teho config v ii_tem intervalu musi odpovidat poctu bodu ii-teho intervalu
				-- => musi se brat gid pro max_ext_version 
				
			-- 2. prepocet NENI MOZNY pokud v db existuji vsechny kategorie dane konfigurace u vsech intervalu [na verzi extenze   ZALEZI]


			-- CYKLUS po jednotlivych INTERVALECH
			FOR ii IN 1..array_length(_step4interval_res,1)
			LOOP
				-- => kontrola add 1.
				WITH
				w1 AS	(
					SELECT tad.* FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_base
					AND tad.config = _config_id_base
					AND tad.gid >= _gid_start_res[ii]
					AND tad.gid <= _gid_end_res[ii]
					),
				w2 AS	(
					SELECT w1.gid, max(w1.ext_version) AS max_ext_version
					FROM w1 GROUP BY w1.gid
					),
				w3 AS	(
					SELECT w1.* FROM w1 INNER JOIN w2
					ON w1.gid = w2.gid
					AND w1.ext_version = w2.max_ext_version
					)
				SELECT count(*) FROM w3
				INTO _check_1;

				IF _check_1 > 0
				THEN
					IF _check_1 = _interval_points_res[ii]
						-- konrola add 1. povoluje prepocet
						-- => v intervalu jsou vsechny body [na verzi extenzi nezalezi]
						-- => dale to pokracuje kontrolou, kde uz na verzi extenze zalezi
					THEN
						-- => kontrola add 2.
						WITH
						w AS	(
							SELECT tad.* FROM @extschema@.t_auxiliary_data AS tad
							WHERE tad.config_collection = _config_collection_base
							AND tad.config = _config_id_base
							AND tad.ext_version = _ext_version_current -- na verzi extenze ZALEZI
							AND tad.gid >= _gid_start_res[ii]
							AND tad.gid <= _gid_end_res[ii]
							)
						SELECT count(*) FROM w
						INTO _check_2;

						IF _check_2 = _interval_points_res[ii]
						THEN
							_check_exists_ii := TRUE; -- interval obsahuje pozadovany pocet bodu v nejaktualnejsi verzi => interval neni kandidatem pro PREPOCET
						ELSE
							_check_exists_ii := FALSE; -- tento interval je kandidatem pro prepocet
						END IF;						
					ELSE
						RAISE EXCEPTION 'Error 24: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany vsechny body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI mozny => pro prepocet nejsou vypocitany vsechny body v ii-tem intervalu ikdyz na verzi extenze nezalezi
					END IF;
				ELSE
					IF _ref_id_total_config_query[i] = 500	-- REFERENCE
					THEN
						RAISE EXCEPTION 'Error 25: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace	UHRNU: _config_collection = %; ID referencni kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany zadne body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI MOZNY => pro prepocet nejsou vypocitany zadne body v ii-tem intervalu
					ELSE
						RAISE EXCEPTION 'Error 26: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany zadne body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI MOZNY => pro prepocet nejsou vypocitany zadne body v ii-tem intervalu
					END IF;
				END IF;

				-- naplneni promenych do RETURN QUERY v cyklu ii
				IF ii = 1
				THEN
					_res_step_ii := array[i];
					_res_config_collection_ii := array[_config_collection];
					_res_config_ii := array[_ref_id_total_categories[i]];
					_res_intersects_ii := array[_intersects];
					_res_config_query_ii := array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := array[ii];
					_res_check_exists_ii := array[_check_exists_ii];
				ELSE
					_res_step_ii := _res_step_ii || array[i];
					_res_config_collection_ii := _res_config_collection_ii || array[_config_collection];
					_res_config_ii := _res_config_ii || array[_ref_id_total_categories[i]];
					_res_intersects_ii := _res_intersects_ii || array[_intersects];
					_res_config_query_ii := _res_config_query_ii || array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := _res_step4interval_ii || array[ii];
					_res_check_exists_ii := _res_check_exists_ii || array[_check_exists_ii];
				END IF;				
			END LOOP;
		END IF;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		-- naplneni promenych do RETURN QUERY v cyklu i
		IF i = 1
		THEN
			_res_step := _res_step_ii;
			_res_config_collection := _res_config_collection_ii;
			_res_config := _res_config_ii;
			_res_intersects := _res_intersects_ii;
			_res_config_query := _res_config_query_ii;
			_res_step4interval := _res_step4interval_ii;
			_res_check_exists := _res_check_exists_ii;
		ELSE
			_res_step := _res_step || _res_step_ii;
			_res_config_collection := _res_config_collection || _res_config_collection_ii;
			_res_config := _res_config || _res_config_ii;
			_res_intersects := _res_intersects || _res_intersects_ii;
			_res_config_query := _res_config_query || _res_config_query_ii;
			_res_step4interval := _res_step4interval || _res_step4interval_ii;
			_res_check_exists := _res_check_exists || _res_check_exists_ii;
		END IF;
									
	END LOOP;

	-------------------------------------------------------------------------------------------
	-- kontrola pro RECOUNT = TRUE
	IF _recount = TRUE
	THEN
		IF	(
			SELECT count(t.res_check_exists_recount) = 0
			FROM (SELECT unnest(_res_check_exists) AS res_check_exists_recount) AS t
			WHERE t.res_check_exists_recount = FALSE
			)
		THEN
			RAISE EXCEPTION 'Error 27: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => vsechny body jsou v aktualni verzi! Poznamka: na verzi extenze zalezi.',_config_collection,_config_collection_base,_config_id_base; -- Prepocet NENI MOZNY => vsechny body jsou v aktualni verzi
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
					
	-------------------------------------------------------------------------------------------
	-- sestaveni vysledneho _q pro RETURN QUERY
	-------------------------------------------------------------------------------------------
	_q :=	'
		WITH
		w1 AS	(
			SELECT
				unnest($1) AS step,
				unnest($2) AS config_collection,
				unnest($3) AS config,
				unnest($4) AS intersects,
				unnest($5) AS config_query,
				unnest($6) AS step4interval,
				unnest($7) AS check_exists
			),
		w2 AS	(
			SELECT
				unnest($8) AS step4interval_res,
				unnest($9) AS gid_start_res,
				unnest($10) AS gid_end_res
			),
		w3 AS	(
			SELECT
				w1.step,
				w1.config_collection,
				w1.config,
				w1.intersects,
				w1.step4interval
			FROM
				w1
			WHERE
				w1.config_query != 500 AND w1.check_exists = FALSE
			)
		SELECT
			w3.step,
			w3.config_collection,
			w3.config,
			w3.intersects,
			w3.step4interval,
			w2.gid_start_res,
			w2.gid_end_res
		FROM
			w3 INNER JOIN w2
		ON
			w3.step4interval = w2.step4interval_res
		ORDER
			BY w3.step, w3.config, w3.step4interval, w2.gid_start_res
		';
	-------------------------------------------------------------------------------------------		
	-------------------------------------------------------------------------------------------
	-- !!! pozor, protinaji se jen tyto konfigurace: (_config_query = 100 a _config_function = ANY(array[100,200]) AND _aggregated = false)
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- kontrola ze _q vraci alespon nejakou konfiguraci pro protinani
	EXECUTE 'WITH w_configs AS ('||_q||') SELECT (count(*))::integer FROM w_configs'
	USING
		_res_step,			-- $1
		_res_config_collection,		-- $2
		_res_config,			-- $3
		_res_intersects,		-- $4
		_res_config_query,		-- $5
		_res_step4interval,		-- $6
		_res_check_exists,		-- $7
		_step4interval_res,		-- $8
		_gid_start_res,			-- $9
		_gid_end_res			-- $10
	INTO
		_check_3;
	-----------------------------------------------------------------------------------
	IF _recount = FALSE AND (_check_3 = 0 OR _check_3 IS NULL)
	THEN
		RAISE EXCEPTION 'Error 28: fn_get_configs4aux_data_app: Pro bodove protinani nenalezeny zadne konfigurace pro protinani. Vse jiz bylo vyprotinano!';
	END IF;
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_step,			-- $1
		_res_config_collection,		-- $2
		_res_config,			-- $3
		_res_intersects,		-- $4
		_res_config_query,		-- $5
		_res_step4interval,		-- $6
		_res_check_exists,		-- $7
		_step4interval_res,		-- $8
		_gid_start_res,			-- $9
		_gid_end_res;			-- $10
	--------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) IS
'Funkce vraci seznam konfiguraci pro funkci fn_get_aux_data_app.';
-- </function>
------------------------------------------------------------------------------;


------------------------------------------------------------------------------;
DROP FUNCTION @extschema@.fn_get_aux_data_app(integer, integer, boolean);
-- <function name="fn_get_aux_data_app" schema="@extschema@" src="functions/extschema/fn_get_aux_data_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_aux_data_app(integer, integer, boolean, integer, integer)
  
CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_data_app
(
	_config_collection 	integer,
	_config			integer,
	_intersects		boolean,
	_gid_start		integer,
	_gid_end		integer
)
RETURNS TABLE
(
	config_collection		integer,
	config				integer,
	gid				integer,
	value				double precision,
	ext_version			integer
) AS
$BODY$
DECLARE
	_ext_version_label_system			text;
	_ext_version_current				integer;
	_config_function_600				integer;
	_ref_id_layer_points				integer;
	_config_collection_4_config			integer;
	_config_query					integer;
	_vector						integer;			
	_raster						integer;
	_raster_1					integer;
	_band						integer;
	_reclass					integer;		
	_condition					character varying;
	_config_function				integer;
	_schema_name					character varying;
	_table_name					character varying;
	_column_ident					character varying;
	_column_name					character varying;
	_unit						double precision;
	_q						text;
	_ids4comb					integer[];
	_config_collection_transform_comb_i		integer;
	_config_collection_transform_comb		integer[];
	_check_gid					integer;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text_complete			text;
	_complete_length				integer;
	_config_ids_complete				integer[];
	_config_ids4check_complete			integer[];
	_complete_exists				boolean[];
	_config_ids_text_categories			text;
	_categories_length				integer;
	_config_ids_categories				integer[];
	_config_ids4check_categories			integer[];
	_categories_exists				boolean[];
	_config_ids					integer[];
	_complete_categories_exists			boolean[];
	_config_collection_transform_i			integer;
	_aggregated					boolean;
	_config_collection_transform			integer[];
	_check_count_config_ids_i			integer;
	_check_count_config_ids				integer[];
	_check_gid_i					integer;
	_config_ids_4_complete_array			integer[];
	_config_collection_4_complete_array		integer[];
	_config_ids_4_categories_array			integer[];
	_config_collection_4_categories_array		integer[];
	_aggregated_200					boolean;
	_check_categories_i				double precision[];	
BEGIN 
	-------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_data_app: Vstupni argument _config_collection nesmi byt NULL!';
	END IF;

	IF _config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_data_app: Vstupni argument _config nesmi byt NULL!';
	END IF;

	IF _intersects IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_data_app: Vstupni argument _intersects nesmi byt NULL!';
	END IF;

	IF _gid_start IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_data_app: Vstupni argument _gid_start nesmi byt NULL!';
	END IF;

	IF _gid_end IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_data_app: Vstupni argument _gid_end nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------
	-------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_data_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_data_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	-------------------------------------------------------------
	-------------------------------------------------------------	
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection
	SELECT
		tcc.config_function,
		tcc.ref_id_layer_points
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		id = _config_collection
	INTO
		_config_function_600,
		_ref_id_layer_points;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function_600 IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection;
	END IF;

	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam ref_id_layer_points v tabulce t_config_collection!',_config_collection;
	END IF;
	-------------------------------------------------------------
	-- kontrola, ze config_function musi byt hodnota 600
	IF _config_function_600 IS DISTINCT FROM 600
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_aux_data_app: Pro vstupni argument config_collection = % nalezen v tabulce t_config_collection zaznam config_function, ktery ale neni hodnota 600!',_config_collection;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config pro _config
	SELECT
		tc.config_collection,
		tc.config_query,
		tc.vector,
		tc.raster,
		tc.raster_1,
		tc.band,
		tc.reclass,		
		tc.condition
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config
	INTO
		_config_collection_4_config,
		_config_query,
		_vector,
		_raster,
		_raster_1,
		_band,
		_reclass,
		_condition;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_collection_4_config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_collection v tabulce t_config!',_config;
	END IF;	
		
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_query v tabulce t_config!',_config;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection_4_config
	SELECT
		tcc.config_function,
		tcc.schema_name,
		tcc.table_name,
		tcc.column_ident,
		tcc.column_name,
		tcc.unit
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection_4_config
	INTO
		_config_function,
		_schema_name,
		_table_name,
		_column_ident,
		_column_name,
		_unit;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 13: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection_4_config;
	END IF;
	-------------------------------------------------------------
	-------------------------------------------------------------
	-------------------------------------------------------------	 
	CASE
	WHEN _config_query = 100 -- vetev pro zakladni protinani
	THEN
		-- kontrola ziskanych promennych pro protinani
		IF _config_function = ANY(array[100,200])
		THEN
			-- _schema_name
			IF ((_schema_name IS NULL) OR (trim(_schema_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 14: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam schema_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _table_name
			IF ((_table_name IS NULL) OR (trim(_table_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 15: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam table_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _column_ident
			IF (_column_ident IS NULL) OR (trim(_column_ident) = '')
			THEN
				RAISE EXCEPTION 'Chyba 16: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam column_ident v tabulce t_config_collection!',_config_collection_4_config;
			END IF;
			
			-- _column_name
			IF (_column_name IS NULL) OR (trim(_column_name) = '')
			THEN
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam column_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- unit
			IF _unit IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 18: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam unit v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- pokud unit neni NULL => vynasobit vzdy hodnotou 10000
			_unit := _unit * 10000.0;
		END IF;
		-----------------------------------------------------------------------------------
		CASE
			WHEN (_config_function = 100)	-- VECTOR
			THEN
				-- raise notice 'PRUCHOD CQ 100 VEKTOR';
				
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN
					_q := @extschema@.fn_sql_aux_data_vector_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_condition,_gid_start,_gid_end);
				ELSE
					RAISE EXCEPTION 'Chyba 19: fn_get_aux_data_app: Jde-li se o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt TRUE!';
				END IF;

			WHEN (_config_function = 200)	-- RASTER
			THEN
				-- raise notice 'PRUCHOD CQ 100 RASTER';
			
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN			
					_q := @extschema@.fn_sql_aux_data_raster_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_band,_reclass,_condition,_unit,_gid_start,_gid_end);
				ELSE
					RAISE EXCEPTION 'Chyba 20: fn_get_aux_data_app: Jde-li se o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt TRUE!';
				END IF;

			WHEN (_config_function IN (300,400))	-- KOMBINACE
			THEN
				-- raise notice 'PRUCHOD CQ 100 KOMBINACE';	
							
				-- spojeni hodnot _vector,_raster,_raster_1 do pole a odstraneni NULL hodnoty z pole
				SELECT array_agg(t.ids)
				FROM (SELECT unnest(array[_vector,_raster,_raster_1]) AS ids) AS t
				WHERE t.ids IS NOT NULL
				INTO _ids4comb;

				IF array_length(_ids4comb,1) != 2
				THEN
					RAISE EXCEPTION 'Chyba 21: fn_get_aux_data_app: Pocet prvku v interni promenne (_ids4comb = %) musi byt 2! Respektive jde-li v konfiguraci o interakci, pak v tabulce t_config u id = % muze byt vyplnena hodnota jen pro kombinaci vector a raster nebo raster a raster_1.',_ids4comb, _config;
				END IF;

				-- proces ziskani ID config_collection pro vector a raster z neagregovane skupiny config_collection 600
				-- a zaroven kontrola PROVEDITELNOSTI, ze pro kombinaci jsou potrebna data jiz ulozena v DB
				
				FOR i IN 1..array_length(_ids4comb,1)
				LOOP
					-- zde v teto casti jde o CQ 100 a kombinaci, proto neni nutne
					-- se ohlizet na aggregated, ale vzdy se musi provest TRANSFORMACE config_collection
					-- pro VxR nebo pro RxR [obecne receno data pro provedeni kombinace uz musi existovat
					-- v 600vkove skupine]
					
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _ids4comb[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_comb_i; 

					IF _config_collection_transform_comb_i IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 22: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					-- kontrola proveditelnoti kombinace
					IF	(
						SELECT count(tad.gid) = 0
						FROM @extschema@.t_auxiliary_data AS tad
						WHERE tad.config_collection = _config_collection_transform_comb_i
						AND tad.config = _ids4comb[i]
						AND tad.ext_version = _ext_version_current
						AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
						)
					THEN
						RAISE EXCEPTION 'Chyba 23: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					IF i = 1
					THEN
						_config_collection_transform_comb := array[_config_collection_transform_comb_i];
					ELSE
						_config_collection_transform_comb := _config_collection_transform_comb || array[_config_collection_transform_comb_i];
					END IF;
				END LOOP;
				
				-- kontrola stejnych gid pro kombinaci
				WITH
				w1 AS	(SELECT tad1.gid FROM @extschema@.t_auxiliary_data AS tad1 WHERE tad1.config_collection = _config_collection_transform_comb[1] AND tad1.config = _ids4comb[1] AND tad1.ext_version = _ext_version_current AND (tad1.gid >= _gid_start AND tad1.gid <= _gid_end)),
				w2 AS	(SELECT tad2.gid FROM @extschema@.t_auxiliary_data AS tad2 WHERE tad2.config_collection = _config_collection_transform_comb[2] AND tad2.config = _ids4comb[2] AND tad2.ext_version = _ext_version_current AND (tad2.gid >= _gid_start AND tad2.gid <= _gid_end)),
				w3 AS	(SELECT w1.gid FROM w1 EXCEPT SELECT w2.gid FROM w2),
				w4 AS	(SELECT w2.gid FROM w2 EXCEPT SELECT w1.gid FROM w1),
				w5 AS	(SELECT w3.gid FROM w3 UNION ALL SELECT w4.gid FROM w4)
				SELECT
					count(w5.gid) FROM w5 INTO _check_gid;

				IF _check_gid > 0
				THEN
					RAISE EXCEPTION 'Chyba 24: fn_get_aux_data_app: Pro konfiguraci config_collection = % a config = % nejsou v tabulce t_auxiliary_data vsechna potrebna data!',_config_collection,_config;
				END IF;

				_q :=
					'				
					WITH
					w1 AS	(SELECT tad1.gid, tad1.value FROM @extschema@.t_auxiliary_data AS tad1 WHERE tad1.config = $1 AND tad1.config_collection = $6  AND tad1.ext_version = $5 AND (tad1.gid >= $12 AND tad1.gid <= $13)),
					w2 AS	(SELECT tad2.gid, tad2.value FROM @extschema@.t_auxiliary_data AS tad2 WHERE tad2.config = $2 AND tad2.config_collection = $11 AND tad2.ext_version = $5 AND (tad2.gid >= $12 AND tad2.gid <= $13)),
					w3 AS	(SELECT tad3.gid AS gid_except FROM @extschema@.t_auxiliary_data AS tad3 WHERE tad3.config = $4 AND tad3.config_collection = $3 AND tad3.ext_version = $5 AND (tad3.gid >= $12 AND tad3.gid <= $13)),
					w4 AS	(
						SELECT
							$3 AS config_collection,
							$4 AS config,
							w1.gid,
							(w1.value * w2.value)::double precision AS value,
							$5 AS ext_version,
							CASE WHEN w3.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
						FROM
								w1
						INNER JOIN 	w2 ON w1.gid = w2.gid
						LEFT  JOIN	w3 ON w1.gid = w3.gid_except
						)
					SELECT
						w4.config_collection,
						w4.config,
						w4.gid,
						w4.value,
						w4.ext_version
					FROM
						w4 WHERE w4.gid_exists = FALSE
					';
			ELSE
				RAISE EXCEPTION 'Chyba 25: fn_get_aux_data_app: Neznama hodnota _config_function = %!',_config_function;
		END CASE;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
	WHEN _config_query IN (200,300,400,500) -- vetev pro soucet, doplnky nebo referenci
	THEN	
		IF _config_query = 500
		THEN
			-- raise notice 'PRUCHOD CQ 500';
			
			RAISE EXCEPTION 'Chyba 26: fn_get_aux_data_app: Jde-li o konfiguraci, ktera je referenci (config_query = %), tak u ni se protinani bodu neprovadi!',_config_query;
		END IF;

		---------------------------------------------------------------
		-- KONTROLA PROVEDITELNOSTI
		---------------------------------------------------------------
		-- pro config_query 200,300,400 => kontrola ze v DB jsou ulozena data pro complete a categories

		-- pokud se jedna o konfiguraci souctu nebo doplnku => pak nutna kontrola,
		-- ze complete nebo categories pro danou konfiguraci jsou jiz v DB pritomny
		-- a odpovidaji pochopitelne pozadovane ext_version_current [poznamka: tato
		-- kontrola je stejna jak pro vetev vypocet, tak i pro vetev prepocet]

		-- pokud nejde o agregovanou skupinu => pak se jako config_collection pouzije VSTUPNI config_collection
		-- pokud   jde o agregovanou skupinu => pak se jako config_collection pouzije transformace

		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------
		-- zjisteni complete a categories
		SELECT
			tc.complete,
			tc.categories
		FROM
			@extschema@.t_config AS tc
		WHERE
			tc.id = _config
		INTO
			_complete,
			_categories;
		-------------------------------------------
		-- _complete muze byt NULL
		-------------------------------------------
		IF _categories IS NULL	-- categories nesmi byt nikdy NULL
		THEN
			RAISE EXCEPTION 'Chyba 27: fn_get_aux_data_app: Pro vstupni argument _config = % nenalezen v tabulce t_config zaznam categories!',_config;
		END IF;
		-------------------------------------------
		-- _complete
		IF _complete IS NOT NULL
		THEN
			-- pridani ke _complete textu array[]
			_config_ids_text_complete := concat('array[',_complete,']');

			-- zjisteni poctu idecek tvorici _complete v
			EXECUTE 'SELECT array_length('||_config_ids_text_complete||',1)'
			INTO _complete_length;

			-- zjisteni existujicich konfiguracnich idecek pro _complete
			EXECUTE
			'SELECT array_agg(tc.id ORDER BY tc.id)
			FROM @extschema@.t_config AS tc
			WHERE tc.id in (select unnest('||_config_ids_text_complete||'))
			'
			INTO _config_ids_complete;

			-- splneni podminky stejneho poctu idecek
			IF _complete_length != array_length(_config_ids_complete,1)
			THEN
				RAISE EXCEPTION 'Chyba 28: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- kontrola zda i obsah je stejny
			EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_complete||') AS ids) AS t'
			INTO _config_ids4check_complete;

			IF _config_ids4check_complete != _config_ids_complete
			THEN
				RAISE EXCEPTION 'Chyba 29: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- sestaveni _complete_exists
			SELECT array_agg(t.complete_exists) FROM (SELECT TRUE AS complete_exists, generate_series(1,_complete_length) AS id) AS t
			INTO _complete_exists;
			
		ELSE
			_complete_length := 0;
			_config_ids_complete := NULL::integer[];
			_complete_exists := NULL::boolean[];
		END IF;
		-------------------------------------------
		-- _categories
		
		-- bude se pracovat zvlast s _categories
		_config_ids_text_categories := concat('array[',_categories,']');

		-- zjisteni poctu idecek tvorici _categories v
		EXECUTE 'SELECT array_length('||_config_ids_text_categories||',1)'
		INTO _categories_length;	-- TOTO

		-- zjisteni existujicich konfiguracnich idecek pro _categories
		EXECUTE
		'SELECT array_agg(tc.id ORDER BY tc.id)
		FROM @extschema@.t_config AS tc
		WHERE tc.id in (select unnest('||_config_ids_text_categories||'))
		'
		INTO _config_ids_categories;

		-- splneni podminky stejneho poctu idecek
		IF _categories_length != array_length(_config_ids_categories,1)
		THEN
			RAISE EXCEPTION 'Chyba 30: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- kontrola zda i obsah je stejny
		EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_categories||') AS ids) AS t'
		INTO _config_ids4check_categories;

		IF _config_ids4check_categories != _config_ids_categories
		THEN
			RAISE EXCEPTION 'Chyba 31: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- sestaveni _categories_exists
		SELECT array_agg(t.categories_exists) FROM (SELECT FALSE AS categories_exists, generate_series(1,_categories_length) AS id) AS t
		INTO _categories_exists;
		-------------------------------------------
		-- spojeni konfiguracnich idecek _complete/_categories
		IF _complete_length = 0
		THEN
			-- jen categories
			_config_ids := _config_ids_categories;
			_complete_categories_exists := _categories_exists;
		ELSE
			-- _complete || _categories
			_config_ids := _config_ids_complete || _config_ids_categories;
			_complete_categories_exists := _complete_exists || _categories_exists;
		END IF;
		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast zjisteni _config_collection_transform_i do POLE a naplneni promenne _complete_categories boolean[]
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)
		LOOP
			-------------------------------------------------
			-------------------------------------------------
			-- proces zjisteni promenne _config_collection_tranform_i:
			IF _complete_categories_exists[i] = TRUE
			THEN			
				-- proces nalezeni transformovane config_collection [TRANSFORMACE]
				SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
				AND tcc.ref_id_layer_points = _ref_id_layer_points
				INTO _config_collection_transform_i;

			ELSE
				SELECT tcc.aggregated FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.id =	(
						SELECT tc.config_collection
						FROM @extschema@.t_config AS tc
						WHERE tc.id = _config
						)
				INTO _aggregated;

				IF _aggregated = TRUE
				THEN
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_i;
				ELSE
					_config_collection_transform_i := _config_collection;
				END IF;
			END IF;
			-------------------------------------------------
			-------------------------------------------------
			-- proces sestaveni _config_collection_transform
			IF i = 1
			THEN
				_config_collection_transform := array[_config_collection_transform_i];
			ELSE
				_config_collection_transform := _config_collection_transform || array[_config_collection_transform_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI --
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)	-- pole _config_ids je spojeni complete/categories, pocet prvku odpovida poctu prvku v _config_collection_transform i _complete_categories
		LOOP										
			SELECT count(tad.id) FROM @extschema@.t_auxiliary_data AS tad
			WHERE tad.config_collection = _config_collection_transform[i]
			AND tad.config = _config_ids[i]
			AND tad.ext_version = _ext_version_current
			AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
			INTO _check_count_config_ids_i;

			IF _check_count_config_ids_i = 0
			THEN
				RAISE EXCEPTION 'Chyba 32: fn_get_aux_data_app: V tabulce t_auxiliary_data pro [nektere complete nebo categories] = % a pro config_collection = % schazi data!',_config_ids[i],_config_collection_transform[i];
			END IF;

			IF i = 1
			THEN
				_check_count_config_ids := array[_check_count_config_ids_i];
			ELSE
				_check_count_config_ids := _check_count_config_ids || array[_check_count_config_ids_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------


		------------------------------------------------------------------------------------------------------
		-- kontrola stejneho poctu bodu u complete a categories
		------------------------------------------------------------------------------------------------------
		IF	(
			SELECT count(tt.ccci) IS DISTINCT FROM 1
			FROM (SELECT DISTINCT t.ccci FROM (SELECT unnest(_check_count_config_ids) AS ccci) AS t) AS tt
			)
		THEN
			RAISE EXCEPTION 'Chyba 33: fn_get_aux_data_app: Pocty bodu v tabulce t_auxiliary_data se pro [nektere complete nebo nektere categories] neshoduji! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
		END IF;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast kontroly stejnych gid
		------------------------------------------------------------------------------------------------------
		IF array_length(_config_ids,1) > 1
		THEN
			-- kontrola stejnych gid pro _config_ids
			FOR i IN 1..(array_length(_config_ids,1) - 1)
			LOOP
				WITH
				w1 AS	(SELECT tad1.gid FROM @extschema@.t_auxiliary_data AS tad1 WHERE tad1.config_collection = _config_collection_transform[1] AND tad1.config = _config_ids[1] AND tad1.ext_version = _ext_version_current AND (tad1.gid >= _gid_start AND tad1.gid <= _gid_end)),
				w2 AS	(SELECT tad2.gid FROM @extschema@.t_auxiliary_data AS tad2 WHERE tad2.config_collection = _config_collection_transform[i+1] AND tad2.config = _config_ids[i+1] AND tad2.ext_version = _ext_version_current AND (tad2.gid >= _gid_start AND tad2.gid <= _gid_end)),
				w3 AS	(SELECT w1.gid FROM w1 EXCEPT SELECT w2.gid FROM w2),
				w4 AS	(SELECT w2.gid FROM w2 EXCEPT SELECT w1.gid FROM w1),
				w5 AS	(SELECT w3.gid FROM w3 UNION ALL SELECT w4.gid FROM w4)
				SELECT
					count(w5.gid) FROM w5
				INTO
					_check_gid_i;

				IF _check_gid_i IS DISTINCT FROM 0
				THEN
					RAISE EXCEPTION 'Chyba 34: fn_get_aux_data_app: U konfigurace config_collection = % a config = % se u kontroly proveditelnosti (cast shoda identifikace bodu indent_point) v tabulce t_auxiliary_data u nektereho complete nebo u nektereho categories neshoduji identifikace bodu gid!',_config_collection, _config;
				END IF;
			END LOOP;
		END IF;	
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------


		------------------------------------------------------------------------------------------------------------------------------
		-- proces rozhozeni pole _config_ids a pole _config_collection_transform zpetne na cast complete a categories
		------------------------------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_complete_categories_exists,1)
		LOOP
			IF _complete_categories_exists[i] = TRUE	-- complete
			THEN
				IF i = 1
				THEN
					_config_ids_4_complete_array := array[_config_ids[i]];
					_config_collection_4_complete_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_complete_array := _config_ids_4_complete_array || array[_config_ids[i]];
					_config_collection_4_complete_array := _config_collection_4_complete_array || array[_config_collection_transform[i]];
				END IF;
			ELSE						-- categories
				IF i = 1
				THEN
					_config_ids_4_categories_array := array[_config_ids[i]];
					_config_collection_4_categories_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_categories_array := _config_ids_4_categories_array || array[_config_ids[i]];
					_config_collection_4_categories_array := _config_collection_4_categories_array || array[_config_collection_transform[i]];
				END IF;
			END IF;
		END LOOP;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_complete_array odpovida _complete_length
		-- kontrola ze delka pole _config_collection_4_complete_array odpovida _complete_length
		IF _config_collection_4_complete_array IS NOT NULL
		THEN
			IF array_length(_config_ids_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 35: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		
			IF array_length(_config_collection_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 36 fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		ELSE
			_config_collection_4_complete_array := NULL::integer[];
		END IF;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_categories_array odpovida _categories_length
		-- kontrola ze delka pole _config_collection_4_categories_array odpovida _categories_length
		IF array_length(_config_ids_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 37: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
			
		IF array_length(_config_collection_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 38: fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
		---------------------------------------------------------------	
		------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------------------------------

		
		---------------------------------------------------------------
		-- CONFIG_QUERY 200 --
										
		IF _config_query = 200
		THEN
			-- raise notice 'PRUCHOD CQ 200';

			---------------------------------------------
			-- kontrola, ze CQ 200 je agregace
			SELECT aggregated FROM @extschema@.t_config_collection
			WHERE id = (SELECT ref_id_total FROM @extschema@.t_config_collection WHERE id = _config_collection)
			INTO _aggregated_200;

			IF _aggregated_200 IS DISTINCT FROM TRUE
			THEN
				RAISE EXCEPTION 'Chyba 39: fn_get_aux_data_app: Jde-li o konfiguraci souctu [config_query = 200], pak musi jit vzdy o agregacni skupinu, respektive v konfiguraci musi platit aggregated = TRUE! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
			END IF;
			---------------------------------------------		

			-- provedeni sumy za categories pro jednotlive body
			_q :=
				'
				WITH
				w1 AS	(
					SELECT
						unnest($10) AS config_collection,
						unnest($9) AS config
					),
				w2 AS	(
					SELECT
						tad.gid,
						tad.value
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w1
					
					ON
						tad.config_collection = w1.config_collection
					AND
						tad.config = w1.config
					AND
						tad.ext_version = $5
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),			
				w3 AS 	(
					SELECT
						w2.gid,
						sum(w2.value)::double precision AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				w4 AS	(
					SELECT tad1.gid AS gid_except
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND tad1.ext_version = $5
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w5 AS	(
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w3.gid,
						w3.value,
						$5 AS ext_version,
						CASE WHEN w4.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
						w3 LEFT JOIN w4 ON w3.gid = w4.gid_except
					)
				SELECT
					w5.config_collection,
					w5.config,
					w5.gid,
					w5.value,
					w5.ext_version
				FROM
					w5 WHERE w5.gid_exists = FALSE
				';			
		END IF;		
		---------------------------------------------------------------
		-- CONFIG_QUERY 300 --
		IF _config_query = 300
		THEN
			-- raise notice 'PRUCHOD CQ 300';
					
			-- kontrola, ze u vsech categories jsou opravdu hodnoty v intervalu 0 az 1
			FOR i IN 1..array_length(_config_ids_4_categories_array,1)
			LOOP		
				SELECT array_agg(t.value ORDER BY t.value)
				FROM	(
					SELECT DISTINCT tad.value::double precision
					FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_4_categories_array[i]
					AND tad.config = _config_ids_4_categories_array[i]
					AND tad.ext_version = _ext_version_current
					AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
					) AS t
				INTO
					_check_categories_i;

				IF _check_categories_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 40 fn_get_aux_data_app: Pro categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % schazi data v tabulce t_auxiliary_data!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
				END IF;

				FOR i IN 1..array_length(_check_categories_i,1)
				LOOP
					IF	(
						_check_categories_i[i] < 0.0	OR
						_check_categories_i[i] > 1.0
						)
					THEN
						RAISE EXCEPTION 'Chyba 41: fn_get_aux_data_app: Hodnoty values u categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % nejsou v intervalu <0.0;1.0>!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
					END IF;
				END LOOP;
				
			END LOOP;

			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				w1 AS	(
					SELECT
						unnest($10) AS config_collection,
						unnest($9) AS config
					),
				w2 AS	(
					SELECT
						tad.gid,
						round(tad.value::numeric,2) AS value
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w1
					
					ON
						tad.config_collection = w1.config_collection
					AND
						tad.config = w1.config
					AND
						tad.ext_version = $5
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				w3 AS	(
					SELECT
						w2.gid,
						(sum(w2.value))::double precision AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				w4 AS	(
					SELECT tad1.gid AS gid_except
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND tad1.ext_version = $5
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w5 AS	(			
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w3.gid,
						(1.0::double precision - w3.value) AS value,
						$5 AS ext_version,
						CASE WHEN w4.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
						w3 LEFT JOIN w4 ON w3.gid = w4.gid_except
					)
				SELECT
					w5.config_collection,
					w5.config,
					w5.gid,
					w5.value,
					w5.ext_version
				FROM
					w5 WHERE w5.gid_exists = FALSE
				';
		END IF;
		---------------------------------------------------------------
		-- CONFIG_QUERY 400 --
		IF _config_query = 400
		THEN
			-- raise notice 'PRUCHOD CQ 400';		

			IF _complete IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 42: fn_get_aux_data_app: Pro konfiguraci kategorie _config = % nenalezena hodnota complete v tabulce t_config!',_config;
			END IF;

			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				-----------------------------------------------
				-- categories
				w1 AS	(
					SELECT
						unnest($10) AS config_collection,
						unnest($9) AS config
					),
				w2 AS	(
					SELECT
						tad.gid,
						(tad.value)::numeric AS value
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w1
					
					ON
						tad.config_collection = w1.config_collection
					AND
						tad.config = w1.config
					AND
						tad.ext_version = $5
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				-----------------------------------------------
				-- complete
				w3 AS	(
					SELECT
						unnest($8) AS config_collection,
						unnest($7) AS config
					),
				w4 AS	(
					SELECT
						tad.gid,
						(tad.value)::numeric AS value
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w3
					
					ON
						tad.config_collection = w3.config_collection
					AND
						tad.config = w3.config
					AND
						tad.ext_version = $5
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),			
				-----------------------------------------------
				w5 AS	(
					SELECT
						w2.gid,
						sum(w2.value) AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				-----------------------------------------------
				w6 AS	(
					SELECT
						w4.gid,
						sum(w4.value) AS value
					FROM
						w4
					GROUP
						BY w4.gid					
					),
				-----------------------------------------------
				w7 AS	(
					SELECT tad1.gid AS gid_except
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND tad1.ext_version = $5
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				-----------------------------------------------
				w8 AS	(
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w6.gid,
						((w6.value - w5.value))::double precision AS value,
						$5 AS ext_version,
						CASE WHEN w7.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
							w6
					INNER JOIN	w5 ON w6.gid = w5.gid
					LEFT  JOIN	w7 ON w6.gid = w7.gid_except
					)
				-----------------------------------------------
				SELECT
					w8.config_collection,
					w8.config,
					w8.gid,
					w8.value,
					w8.ext_version
				FROM
					w8 WHERE w8.gid_exists = FALSE
				';	
		
		END IF;
		---------------------------------------------------------------	
	ELSE
		RAISE EXCEPTION 'Chyba 43: fn_get_aux_data_app: Neznama hodnota _config_query = %!',_config_query;
	END CASE;
	-------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_ids4comb[1],				-- $1
		_ids4comb[2],				-- $2
		_config_collection,			-- $3
		_config,				-- $4
		_ext_version_current,			-- $5
		_config_collection_transform_comb[1],	-- $6
		_config_ids_4_complete_array,		-- $7
		_config_collection_4_complete_array,	-- $8
		_config_ids_4_categories_array,		-- $9
		_config_collection_4_categories_array,	-- $10
		_config_collection_transform_comb[2],	-- $11
		_gid_start,				-- $12
		_gid_end;				-- $13		
	-------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) IS
'Funkce vraci data pro insert do tabulky t_auxiliary_data.';
-- </function>
------------------------------------------------------------------------------;


------------------------------------------------------------------------------;
DROP FUNCTION @extschema@.fn_sql_aux_data_points_app(integer);
-- <function name="fn_sql_aux_data_points_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_data_points_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_points_app
(
	_ref_id_layer_points		integer,
	_gid_start			integer,
	_gid_end			integer,
	_config_collection		integer,
	_config				integer
)
RETURNS text AS
$BODY$
DECLARE
	_ext_version_label_system	text;
	_ext_version_current		integer;
	_condition4plot			text;
	_condition4aux			text;
	_result				text;
	_check				integer;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_pts
	IF (_ref_id_layer_points IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_points_app: Hodnota parametru _ref_id_layer_points nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- gid_start
	IF (_gid_start IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_points_app: Hodnota parametru _gid_start nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- gid_end
	IF (_gid_end IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_points_app: Hodnota parametru _gid_end nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- config_collection
	IF (_config_collection IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_points_app: Hodnota parametru config_collection nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- config
	IF (_config IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_points_app: Hodnota parametru config nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_data_points_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_data_points_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;
	-----------------------------------------------------------------------------------	
	-- condition4plot
	_condition4plot := concat('(gid >= ',_gid_start,' AND gid <= ',_gid_end,')');
	-----------------------------------------------------------------------------------
	-- condition4aux
	_condition4aux := concat('(gid >= ',_gid_start,' AND gid <= ',_gid_end,')');
	-----------------------------------------------------------------------------------
	_result :=
		'
		SELECT
			t3.gid,
			t3.geom
		FROM
			(
			SELECT
				t1.gid,
				t1.geom,
				t2.gid AS ident_point
			FROM
				(
				SELECT
					gid,
					geom
				FROM
					@extschema@.f_p_plot
				WHERE
					config_collection = #CONFIG_COLLECTION_PLOT#
				AND
					#CONDITION4PLOT#
				) AS t1
			LEFT
			JOIN	(
				SELECT gid
				FROM @extschema@.t_auxiliary_data
				WHERE config_collection = #CONFIG_COLLECTION_AUX#
				AND config = #CONFIG#
				AND ext_version = #EXT_VERSION_CURRENT#
				AND #CONDITION4AUX#
				) AS t2
			ON
				t1.gid = t2.gid
			) AS t3
		WHERE
			t3.ident_point IS NULL
		';

	-- nahrazeni promennych casti v dotazu
	_result := replace(_result, '#CONFIG_COLLECTION_PLOT#', concat(_ref_id_layer_points));
	_result := replace(_result, '#CONDITION4PLOT#', _condition4plot);
	_result := replace(_result, '#CONFIG_COLLECTION_AUX#', concat(_config_collection));
	_result := replace(_result, '#CONFIG#', concat(_config));
	_result := replace(_result, '#EXT_VERSION_CURRENT#', concat(_ext_version_current));
	_result := replace(_result, '#CONDITION4AUX#', _condition4aux);
	-----------------------------------------------------------------------------------
	-- kontrola ze _result vrati alespon nejaky bod pro protinani
	EXECUTE 'WITH w_points AS ('||_result||') SELECT (count(*))::integer FROM w_points'
	INTO _check;
	-----------------------------------------------------------------------------------
	IF (_check = 0 OR _check IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_data_points_app: Pro bodove protinani nenalezeny patricne body.';
	END IF;
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

ALTER FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer)
IS 'Funkce vrací SQL textový řetězec pro ziskani seznamu bodu pro bodove protinani.';
-- </function>
------------------------------------------------------------------------------;


------------------------------------------------------------------------------;
DROP FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision);
-- <function name="fn_sql_aux_data_raster_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_data_raster_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_raster_app
(
	config_collection		integer,
	config				integer,
	schema_name			character varying,
	table_name			character varying,
	column_ident			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	gid_start			integer,
	gid_end				integer
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	_ref_id_layer_points	integer;
	_q			text;
	_column_text		text;
	_result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_collection
	IF config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_raster_app: Hodnota parametru config_collection nesmí být NULL.';
	END IF;

	-- config
	IF config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_raster_app: Hodnota parametru config nesmí být NULL.';
	END IF;
	
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_raster_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_raster_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_ident
	IF ((column_ident IS NULL) OR (trim(column_ident) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_raster_app: Hodnota parametru column_ident nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_data_raster_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_data_raster_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_data_raster_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- gid_start 
	IF (gid_start IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_data_raster_app: Hodnota parametru gid_start nesmí být NULL.';
	END IF;

	-- gid_end 
	IF (gid_end IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_sql_aux_data_raster_app: Hodnota parametru gid_end nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- zjisteni reference pro bodovou vrstvu
	SELECT tcc.ref_id_layer_points FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = config_collection
	INTO _ref_id_layer_points;
	-----------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_sql_aux_data_raster_app: Nenalezena hodnota ref_id_layer_points pro config_collection = %.',config_collection;
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni vnoreneho dotazu pro vrstvu bodu
	_q := (SELECT * FROM @extschema@.fn_sql_aux_data_points_app(_ref_id_layer_points,gid_start,gid_end,config_collection,config));
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro protnuti bodu z vrstvou
	_result :=
		'
		WITH                                                                    
		w_point_geom AS	('||_q||'),
		-------------------------------------------------------------------
		w_intersects AS
				(
				SELECT
					t1.gid,
					CASE
					WHEN t2.#COLUMN_NAME# IS NULL THEN 0.0
					ELSE #VALUE#
					END AS value
				FROM
					w_point_geom AS t1					
				LEFT JOIN
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					ST_Convexhull(t2.#COLUMN_NAME#) && t1.geom
				AND
					ST_Intersects(ST_Convexhull(t2.#COLUMN_NAME#),t1.geom)
				AND
					#CONDITION#
				)
		-------------------------------------------------------------------			
		SELECT
			#CONFIG_COLLECTION# AS config_collection,
			#CONFIG# AS config,
			gid,
			coalesce((value::double precision * #UNIT#),0.0::double precision) AS value,
			$5 AS ext_version
		FROM
			w_intersects;
		';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass IS NULL)
	THEN
		_column_text := 't2.#COLUMN_NAME#';
	ELSE
		_column_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME#,#BAND#,ST_BandPixelType(t2.#COLUMN_NAME#,#BAND#),#RECLASS_VALUE#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	_result := replace(_result, '#VALUE#', concat('ST_Value(',_column_text,',#BAND#,t1.geom)'));
	_result := replace(_result, '#CONFIG_COLLECTION#', config_collection::character varying);
	_result := replace(_result, '#CONFIG#', config::character varying);
	_result := replace(_result, '#SCHEMA_NAME#', schema_name);
	_result := replace(_result, '#TABLE_NAME#', table_name);
	_result := replace(_result, '#COLUMN_NAME#', column_name);
	_result := replace(_result, '#BAND#', band::character varying);
	_result := replace(_result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	_result := replace(_result, '#UNIT#', unit::character varying);

	IF (reclass IS NOT NULL)
	THEN
		_result := replace(_result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$$;

ALTER FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
IS
'Funkce vrací SQL textový řetězec pro ziskani pomocné proměnné z rasterové vrstvy protinanim bodu.';
-- </function>
------------------------------------------------------------------------------;


------------------------------------------------------------------------------;
DROP FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying);
-- <function name="fn_sql_aux_data_vector_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_data_vector_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_vector_app
(
	config_collection	integer,
	config			integer,
	schema_name		character varying,
	table_name		character varying,
	column_ident		character varying,
	column_name		character varying,
	condition		character varying,
	gid_start		integer,
	gid_end			integer
)
RETURNS text AS
$BODY$
DECLARE
	_ref_id_layer_points	integer;
	_q			text;
	_result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_collection
	IF (config_collection IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_vector_app: Hodnota parametru config_collection nesmí být NULL.';
	END IF;
		
	-- config
	IF (config IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_vector_app: Hodnota parametru config nesmí být NULL.';
	END IF;
	
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_vector_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_vector_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_ident
	IF (column_ident IS NULL) OR (trim(column_ident) = '')
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_vector_app: Hodnota parametru column_ident nesmí být NULL.';
	END IF;

	-- column_name
	IF (column_name IS NULL) OR (trim(column_name) = '')
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_data_vector_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF (condition IS NULL) OR (trim(condition) = '')
	THEN
		condition := NULL;
	END IF;

	-- gid_start 
	IF (gid_start IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_data_vector_app: Hodnota parametru gid_start nesmí být NULL.';
	END IF;

	-- gid_end 
	IF (gid_end IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_data_vector_app: Hodnota parametru gid_end nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- zjisteni reference pro bodovou vrstvu
	SELECT tcc.ref_id_layer_points FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = config_collection
	INTO _ref_id_layer_points;
	-----------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_data_vector_app: Nenalezena hodnota ref_id_layer_points pro config_collection = %.',config_collection;
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni vnoreneho dotazu pro vrstvu bodu
	_q := (SELECT * FROM @extschema@.fn_sql_aux_data_points_app(_ref_id_layer_points,gid_start,gid_end,config_collection,config));
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro protnuti bodu z vrstvou
	_result :=
		'
		WITH                                                                    
		w_point_geom AS	('||_q||'),
		w_intersects AS
				(
				SELECT
					t1.gid,
					CASE WHEN t2.#COLUMN_IDENT# IS NOT NULL THEN 1 ELSE 0 END AS value
				FROM
					w_point_geom AS t1
				LEFT JOIN
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					ST_Intersects(t2.#COLUMN_NAME#,t1.geom)
				AND
					#CONDITION#
				)			
		SELECT
			#CONFIG_COLLECTION# AS config_collection,
			#CONFIG# AS config,
			gid,
			coalesce(value::double precision,0.0::double precision) AS value,
			$5 AS ext_version
		FROM
			w_intersects;
		';
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	_result := replace(_result, '#CONFIG_COLLECTION#', config_collection::character varying);
	_result := replace(_result, '#CONFIG#', config::character varying);
	_result := replace(_result, '#SCHEMA_NAME#', schema_name);
	_result := replace(_result, '#TABLE_NAME#', table_name);
	_result := replace(_result, '#COLUMN_IDENT#', column_name);
	_result := replace(_result, '#COLUMN_NAME#', column_name);
	_result := replace(_result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

ALTER FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying, integer, integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying, integer, integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying, integer, integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying, integer, integer)
IS 'Funkce vrací SQL textový řetězec pro ziskani pomocné proměnné z vektorové vrstvy protnutim inventarizacnich bodu.';
-- </function>
------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- drop tables
---------------------------------------------------------------------------------------------------;
DROP TABLE IF EXISTS @extschema@.r_ndsm_smooth CASCADE;
DROP TABLE IF EXISTS @extschema@.r_s2_ftype_smooth CASCADE;
DROP TABLE IF EXISTS @extschema@.r_s2_species_smooth CASCADE;
DROP TABLE IF EXISTS @extschema@.c_lai_decrease_bands CASCADE;
DROP TABLE IF EXISTS @extschema@.c_lai_decrease_category CASCADE;
DROP TABLE IF EXISTS @extschema@.c_s2_ftype_bands CASCADE;
DROP TABLE IF EXISTS @extschema@.c_s2_ftype_category CASCADE;
DROP TABLE IF EXISTS @extschema@.c_s2_ftype_smooth_bands CASCADE;
DROP TABLE IF EXISTS @extschema@.c_s2_species_bands CASCADE;
DROP TABLE IF EXISTS @extschema@.c_s2_species_category CASCADE;
DROP TABLE IF EXISTS @extschema@.c_s2_species_smooth_bands CASCADE;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- drop function
---------------------------------------------------------------------------------------------------;
DROP FUNCTION IF EXISTS @extschema@.fn_get_r_ndsm_smooth_rids(_rids integer[], _syear integer);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- changes in f_p_plot
---------------------------------------------------------------------------------------------------;
ALTER TABLE @extschema@.f_p_plot DROP COLUMN geom;
ALTER TABLE @extschema@.f_p_plot ADD COLUMN geom geometry(Point);
ALTER TABLE @extschema@.f_p_plot ALTER COLUMN geom SET NOT NULL;
COMMENT ON COLUMN @extschema@.f_p_plot.geom IS 'Plot coordinates, point geometry.';
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- fn_sql_aux_total_raster_comb_app
---------------------------------------------------------------------------------------------------;
-- <function name="fn_sql_aux_total_raster_comb_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_total_raster_comb_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_raster_comb_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_raster_comb_app(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	schema_name_1			character varying,
	table_name_1			character varying,
	column_name_1			character varying,
	band_1				integer,
	reclass_1			integer,
	condition_1			character varying,
	unit_1				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	reclass_val_column	text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_raster_comb_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_raster_comb_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_raster_comb_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_raster_comb_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_raster_comb_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- schema_name_1
	IF ((schema_name_1 IS NULL) OR (trim(schema_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_raster_comb_app: Hodnota parametru schema_name_1 nesmí být NULL.';
	END IF;

	-- table_name_1
	IF ((table_name_1 IS NULL) OR (trim(table_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_total_raster_comb_app: Hodnota parametru table_name_1 nesmí být NULL.';
	END IF;

	-- column_name_1
	IF ((column_name_1 IS NULL) OR (trim(column_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_total_raster_comb_app: Hodnota parametru column_name_1 nesmí být NULL.';
	END IF;

	-- band_1
	IF (band_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_total_raster_comb_app: Hodnota parametru band_1 nesmí být NULL.';
	END IF;

	-- condition_1 [povoleno NULL]
	IF ((condition_1 IS NULL) OR (trim(condition_1) = ''))
	THEN
		condition_1 := NULL;
	END IF;

	-- unit_1
	IF (unit_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_sql_aux_total_raster_comb_app: Hodnota parametru unit_1 nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
	'
	WITH
	---------------------------------------------------------------
	w_cell AS	(-- vyber uzemi
			SELECT
				geom AS geom_cell,
				estimation_cell
			FROM
				@extschema@.f_a_cell
			WHERE
				gid = #GID#
			)
	---------------------------------------------------------------
	,w_cell_rast2 AS materialized
			(-- protnuti vybraneho uzemi z rasterem
			SELECT
				t1.geom_cell,
				t1.estimation_cell,
				t2.#COLUMN_NAME# AS rast4dump
			FROM
				w_cell AS t1
			LEFT
			JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2

			ON	t1.geom_cell && st_convexhull(t2.#COLUMN_NAME#)
			
			AND	ST_Intersects(t1.geom_cell, st_convexhull(t2.#COLUMN_NAME#))
			
			AND	#CONDITION#
			)
	---------------------------------------------------------------
	,w_polygons AS materialized
			(-- rozdumpovani rasteru na polygony (nemusi byt pokryt cely katastr, kvuli 0 tileum)
			SELECT
				geom_cell,
				estimation_cell,
				ST_DumpAsPolygons(rast4dump, #BAND#) AS rast_poly
			FROM
				w_cell_rast2
			WHERE
				rast4dump IS NOT NULL
			)
	---------------------------------------------------------------
	,w_dumps AS materialized
			(
			SELECT
				geom_cell,
				estimation_cell,
				(rast_poly).val,
				(rast_poly).geom
			FROM
				w_polygons 
			WHERE
				#RECLASS#
			)
	---------------------------------------------------------------
	,w_intersection AS materialized
			(
			SELECT
				geom_cell,
				estimation_cell,
				#RECLASS_VAL_COLUMN# * #UNIT# AS val_reclass,				
				ST_Intersection (geom_cell, geom) AS geom
			FROM
				w_dumps
			)
	---------------------------------------------------------------
	,w_cell_rast1 as materialized
			(-- protnuti rasteru1 z rozdampovanyma geometriema withu w_intersection
			SELECT
				geom_cell,
				estimation_cell,
				val_reclass,
				ST_Intersection(t1.geom, t2.#COLUMN_NAME_1#) AS res_intersects
			FROM
				w_intersection AS t1
			LEFT JOIN
				#SCHEMA_NAME_1#.#TABLE_NAME_1# AS t2
			
			ON	t1.geom && st_convexhull(t2.#COLUMN_NAME_1#)
			
			AND	ST_Intersects(t1.geom, st_convexhull(t2.#COLUMN_NAME_1#))
			
			AND	#CONDITION_1#
			
			WHERE
				ST_IsEmpty(t1.geom) = false
			)
	---------------------------------------------------------------
	,w_dump_all as materialized
			(-- rozbaleni intersection ndsm na geometrii a hodnotu
			SELECT 
				estimation_cell,
				val_reclass,
				(res_intersects).geom AS res_geom,
				(res_intersects).val AS ndsm_val
			FROM
				w_cell_rast1
			)
	---------------------------------------------------------------
	,w_sum_part AS	(
			select
				sum((ST_Area(res_geom)) * val_reclass * ndsm_val) as sum_part
			FROM
				w_dump_all
			)
	---------------------------------------------------------------
	,w_sum AS 	(
			-- tady se protnul ndsm s ftype
			-- tam kde cast katastru byla pokryta ftype 0 (chybeji tile) se neprovede nic,
			-- tyto pixely jsou odstraneny jiz na zacatku
			
			SELECT 
				sum_part
			FROM 
				w_sum_part
			-------------------------------------
			UNION ALL
			-------------------------------------
			-- pripojeni katastru, ktere maji ftype vsude 0
			
			SELECT
				0.0 AS sum_part
			FROM
				w_cell_rast2
			WHERE
				rast4dump IS NULL
			-------------------------------------
			UNION ALL
			-------------------------------------
			-- pripojeni geometrie, ktere jsou pokryte nejakym tilem, ale vsude je 0

			SELECT
				0.0 AS sum_part
			FROM
				w_polygons
			WHERE
				(rast_poly).val = 0
			)
	---------------------------------------------------------------
	SELECT 
		coalesce(sum(sum_part),0) AS aux_total_#CONFIG_ID#_#GID#
	FROM 
		w_sum;
	';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace pro raster v prvnim poradi protinani
	IF (reclass IS NULL)
	THEN
		reclass_text := '(rast_poly).val != 0';			-- defaultni nastaveni
		reclass_val_column := 'val';				-- val hodnota se zde nepreklasifikovava [bere se sloupec val]
	ELSE
		reclass_text := '(rast_poly).val = #RECLASS_VALUE#';	-- hodnota val urci s jakymi rozdampovanymi polygony se bude dale pracovat [ty si nesou hodnotu val z prvniho protinani]
		reclass_val_column := '1.0';				-- hodnota val z prvniho protinani se umele preklasifikuje vzdy na hodnotu 1.0
	END IF;
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace pro raster ve druhem poradi protinani
	IF (reclass_1 IS NOT NULL)
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_sql_aux_total_raster_comb_app: Neznama reklasifikace pro druhy raster v protinani!'; -- momentalne jako raster pro druhe protinani je vrstva ndsm u ktere se reklasifikace neprovadi
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS#', reclass_text);
	result := replace(result, '#RECLASS_VAL_COLUMN#', reclass_val_column);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#BAND#', band::character varying);	
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#SCHEMA_NAME_1#', schema_name_1);
	result := replace(result, '#TABLE_NAME_1#', table_name_1);
	result := replace(result, '#COLUMN_NAME_1#', column_name_1);
	result := replace(result, '#BAND_1#', band_1::character varying);
	result := replace(result, '#CONDITION_1#', coalesce(condition_1::character varying, 'TRUE'));
	result := replace(result, '#UNIT_1#', unit_1::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové kategorie v rámci vektorové vrstvy.';

--------------------------------------------------------------------------------;

-- </function>
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- <function name="fn_t_config_collection__before_insert" schema="@extschema@" src="functions/extschema/fn_t_config_collection__before_insert.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_t_config_collection__before_insert
--------------------------------------------------------------------------------;

	DROP FUNCTION IF EXISTS @extschema@.fn_t_config_collection__before_insert() CASCADE;

	CREATE OR REPLACE FUNCTION @extschema@.fn_t_config_collection__before_insert()
	RETURNS trigger AS
	$$
	DECLARE
		_condition	varchar;
	BEGIN
		IF (NEW.config_function = 500)
		THEN
			
			IF ((NEW.condition IS NULL) OR (trim(NEW.condition) = '')) 
			THEN
				_condition := 'TRUE'::varchar;
			ELSE
				_condition := NEW.condition;
			END IF;
			
			EXECUTE
				'SELECT coalesce(count(*),0) '||
				'FROM '||NEW.schema_name||'.'||NEW.table_name||' '||
				'WHERE '||_condition||';'
			INTO NEW.total_points;

			IF (NEW.total_points = 0)
			THEN
				RAISE EXCEPTION 'Error 01: fn_t_config_collection__before_insert: The value of "total_points" cannot by zero.';
			END IF;
		ELSE
			NEW.total_points := NULL::integer;
		END IF;
		RETURN NEW;
	END;
	$$
	LANGUAGE plpgsql
	VOLATILE
	SECURITY INVOKER;

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	ALTER FUNCTION @extschema@.fn_t_config_collection__before_insert()
	OWNER TO adm_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO adm_nfiesta_gisdata;

	GRANT EXECUTE
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO app_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO public;

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	COMMENT ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	IS
	'Funkce triggeru, před vložením záznamu do tabulky t_config_collection provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';

--------------------------------------------------------------------------------;
-- </function>
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- <function name="fn_t_config_collection__before_update" schema="@extschema@" src="functions/extschema/fn_t_config_collection__before_update.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_t_config_collection__before_update
--------------------------------------------------------------------------------;

	DROP FUNCTION IF EXISTS @extschema@.fn_t_config_collection__before_update() CASCADE;

	CREATE FUNCTION @extschema@.fn_t_config_collection__before_update()
	RETURNS trigger
	AS $$
	DECLARE
		_condition	varchar;
	BEGIN
		IF (NEW.config_function = 500)
		THEN
			IF ((NEW.condition IS NULL) OR (trim(NEW.condition) = '')) 
			THEN
				_condition := 'TRUE'::varchar;
			ELSE
				_condition := NEW.condition;
			END IF;
			
			EXECUTE
				'SELECT coalesce(count(*),0) '||
				'FROM '||NEW.schema_name||'.'||NEW.table_name||' '||
				'WHERE '||_condition||';'
			INTO NEW.total_points;

			IF (NEW.total_points = 0)
			THEN
				RAISE EXCEPTION 'Error 01: fn_t_config_collection__before_update: The value of "total_points" cannot by zero.';
			END IF;
		ELSE
			NEW.total_points := NULL::integer;
		END IF;
		RETURN NEW;
	END;
	$$
	LANGUAGE plpgsql
	VOLATILE
	SECURITY INVOKER;

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	ALTER FUNCTION @extschema@.fn_t_config_collection__before_update()
	OWNER TO adm_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	TO adm_nfiesta_gisdata;

	GRANT EXECUTE
	ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	TO app_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	TO public;

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	COMMENT ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	IS
	'Funkce triggeru, před změnou záznamu v tabulce t_config_collection provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';
	
-- </function>
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- create triggers
---------------------------------------------------------------------------------------------------;
CREATE TRIGGER trg__t_config_collection__before_insert
  BEFORE INSERT
  ON @extschema@.t_config_collection
  FOR EACH ROW
  EXECUTE PROCEDURE @extschema@.fn_t_config_collection__before_insert();
  
COMMENT ON TRIGGER trg__t_config_collection__before_insert ON @extschema@.t_config_collection IS
'Trigger spouští před vložením nového záznamu do tabulky t_config_collection funkci fn_t_config_collection__before_insert, která provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';

CREATE TRIGGER trg__t_config_collection__before_update
  BEFORE UPDATE
  ON @extschema@.t_config_collection
  FOR EACH ROW
  EXECUTE PROCEDURE @extschema@.fn_t_config_collection__before_update();
  
COMMENT ON TRIGGER trg__t_config_collection__before_update ON @extschema@.t_config_collection IS
'Trigger spouští před změnou záznamu v tabulce t_config_collection funkci fn_t_config_collection__before_update, která provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- delete check constraints
---------------------------------------------------------------------------------------------------;
ALTER TABLE @extschema@.t_config_collection DROP CONSTRAINT check__t_config_collection__not_null__condition;
ALTER TABLE @extschema@.t_config_collection DROP CONSTRAINT check__t_config_collection__not_null__column_ident;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- set NULL for column ident_point
---------------------------------------------------------------------------------------------------;
ALTER TABLE @extschema@.t_auxiliary_data ALTER COLUMN ident_point DROP NOT NULL;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- <function name="fn_auxiliary_data_before_insert_app" schema="@extschema@" src="functions/extschema/fn_auxiliary_data_before_insert_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- Function: @extschema@.fn_auxiliary_data_before_insert_app()

-- DROP FUNCTION @extschema@.fn_auxiliary_data_before_insert_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_auxiliary_data_before_insert_app()
  RETURNS trigger AS
$BODY$
	DECLARE
		_ext_version_label_system	text;
		_ext_version_id_system		integer;
		_ext_version_label		character varying;
	BEGIN
		-------------------------------------------------------------------------
		-- cast SYSTEMOVA --
		-------------------------------------------------------------------------
		SELECT extversion FROM pg_extension
		WHERE extname = 'nfiesta_gisdata'
		INTO _ext_version_label_system;
			
		IF _ext_version_label_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 01: fn_auxiliary_data_before_insert_app: V systemove tabulce pg_extension nenalezena zadna verze extenze nfiesta_gisdata!';
		END IF;
	
		SELECT id FROM @extschema@.c_ext_version
		WHERE label = _ext_version_label_system
		INTO _ext_version_id_system;
	
		IF _ext_version_id_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 02: fn_auxiliary_data_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		-- cast INSERTOVANA
		-------------------------------------------------------------------------
		SELECT label FROM @extschema@.c_ext_version WHERE id = NEW.ext_version
		INTO _ext_version_label;

		IF _ext_version_label IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 03: fn_auxiliary_data_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici insertovane verzi % extenze nfiesta_gisdata!',_ext_version_label;
		END IF;
		-------------------------------------------------------------------------
		-- cast KONTROLY
		-------------------------------------------------------------------------
		IF NEW.ext_version > _ext_version_id_system
		THEN
			RAISE EXCEPTION 'Chyba 04: fn_auxiliary_data_before_insert_app: Insertovana verze extenze (ext_version = %) nesmi byt novejsi, nez je prave aktualni systemova verze (%) extenze nfiesta_gisdata!',_ext_version_label,_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		RETURN NEW;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION @extschema@.fn_auxiliary_data_before_insert_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_auxiliary_data_before_insert_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_auxiliary_data_before_insert_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_auxiliary_data_before_insert_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_auxiliary_data_before_insert_app() IS
'Funkce provádí kontrolu importovaného záznamu verze extenze (ext_version). Spouští se triggerem before insert tabulky t_auxiliary_data.';

-- </function>
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- create triggers
---------------------------------------------------------------------------------------------------;
CREATE TRIGGER trg__t_auxiliary_data__before_insert
  BEFORE INSERT
  ON @extschema@.t_auxiliary_data
  FOR EACH ROW
  EXECUTE PROCEDURE @extschema@.fn_auxiliary_data_before_insert_app();
  
COMMENT ON TRIGGER trg__t_auxiliary_data__before_insert ON @extschema@.t_auxiliary_data IS
'Trigger před vložením nového záznamu spouští funkci fn_auxiliary_data_before_insert_app(), která provádí kontrolu nově vkládane verze extenze s aktulani systemovou verzi extenze nfiesta_gisdata.';
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- delete function
---------------------------------------------------------------------------------------------------;
DROP FUNCTION @extschema@.fn_check_versions_app(integer);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- dumps for tables
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_gui_version', '{WHERE NOT (id BETWEEN 100 AND 500)}');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_ext_gui_version', '{WHERE NOT (id BETWEEN 1 AND 12)}');
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- dumps for sequences
---------------------------------------------------------------------------------------------------;
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_ext_gui_version_id_seq', '{WHERE NOT (id BETWEEN 1 AND 12)}');
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- <function name="fn_sql_aux_total_vector_comb_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_total_vector_comb_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_vector_comb_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	condition			character varying,
	unit				double precision,
	schema_name_1			character varying,
	table_name_1			character varying,
	column_name_1			character varying,
	band_1				integer,
	reclass_1			integer,
	condition_1			character varying,
	unit_1				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF (column_name IS NULL) OR (trim(column_name) = '')
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- condition
	-- povoleno NULL
	IF (condition IS NULL) OR (trim(condition) = '')
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_vector_comb_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- schema_name_1
	IF ((schema_name_1 IS NULL) OR (trim(schema_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_vector_comb_app: Hodnota parametru schema_name_1 nesmí být NULL.';
	END IF;

	-- table_name_1
	IF ((table_name_1 IS NULL) OR (trim(table_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_vector_comb_app: Hodnota parametru table_name_1 nesmí být NULL.';
	END IF;

	-- column_name_1
	IF ((column_name_1 IS NULL) OR (trim(column_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_total_vector_comb_app: Hodnota parametru column_name_1 nesmí být NULL.';
	END IF;

	-- band_1
	IF (band_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_total_vector_comb_app: Hodnota parametru band_1 nesmí být NULL.';
	END IF;

	-- condition_1 [povoleno NULL]
	IF ((condition_1 IS NULL) OR (trim(condition_1) = ''))
	THEN
		condition_1 := NULL;
	END IF;

	-- unit_1
	IF (unit_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_total_vector_comb_app: Hodnota parametru unit_1 nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
	'
	WITH
	--------------------------------------------------------------- 
	w_ku_olil AS materialized

			(-- propojeni tabulek olilu a katastru na zaklade jejich pruniku, vektorová operace
			SELECT
				t1.gid, 
				t1.estimation_cell,
				t1.geom as geom_cell,
				t2.#COLUMN_NAME#,
				ST_Intersection(t1.geom, t2.#COLUMN_NAME#) as intersect_ku_olil  -- vysledna geometrie protnuti geometrie GIDu a OLILu
			FROM
				@extschema@.f_a_cell AS t1
			LEFT JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2
			ON
				ST_Intersects(t2.#COLUMN_NAME#,t1.geom) 
			AND 
				#CONDITION#
			WHERE 
				t1.gid = #GID#
			)
	---------------------------------------------------------------
	---------------------------------------------------------------
	,w_ndsm_olil_katastr_0 AS	(
					SELECT
						t1.*,
						t2.*,
						CASE
							WHEN t2.#COLUMN_NAME_1# IS NULL THEN t2.#COLUMN_NAME_1#
							ELSE #RECLASS_1#
						END
							AS rast_reclass
					FROM
						w_ku_olil AS t1
					LEFT
					JOIN	#SCHEMA_NAME_1#.#TABLE_NAME_1# AS t2

					ON	t1.intersect_ku_olil && ST_Convexhull(t2.#COLUMN_NAME_1#)

					AND	ST_Intersects(t1.intersect_ku_olil,ST_Convexhull(t2.#COLUMN_NAME_1#))
					)
	,w_ndsm_olil_katastr AS		(
					SELECT
						gid,
						estimation_cell,
						rast_reclass,
						intersect_ku_olil,
						CASE WHEN rast_reclass IS NULL THEN false ELSE true END AS ident_null
					FROM
						w_ndsm_olil_katastr_0 as A
					WHERE
						#CONDITION_1#
					AND
						(
						ST_GeometryType(A.intersect_ku_olil) = ''ST_Polygon'' OR
						ST_GeometryType(A.intersect_ku_olil) = ''ST_MultiPolygon''
						)
					)
	---------------------------------------------------------------
	---------------------------------------------------------------
	,w_covers AS materialized
			(
			SELECT
				gid, 
				estimation_cell, 
				rast_reclass, 
				intersect_ku_olil,
				ST_Covers(intersect_ku_olil, ST_Convexhull(rast_reclass)) AS covers
			FROM
				w_ndsm_olil_katastr
			WHERE
				ident_null
			)
	---------------------------------------------------------------
	,w_uhrn AS	(
			SELECT
				estimation_cell,
				(ST_ValueCount(rast_reclass, #BAND_1#, true)) AS val_count,
				(ST_PixelWidth(rast_reclass) * ST_PixelHeight(rast_reclass))*#UNIT_1# AS pixarea
			FROM
				w_covers
			WHERE
				covers = true
			)
	---------------------------------------------------------------
	,w_uhrn_not AS	(
			SELECT
				estimation_cell,
				ST_Intersection(intersect_ku_olil,rast_reclass) AS vector_intersect_record
			FROM
				w_covers
			WHERE
				covers = false
			)
	---------------------------------------------------------------
	,w_sum AS	(
			SELECT
				SUM(((ST_Area((vector_intersect_record).geom))*#UNIT#)*((vector_intersect_record).val)) AS sum_part
			FROM 
				w_uhrn_not
			-------------------
			UNION ALL
			-------------------
			SELECT 
				SUM((val_count).value*(val_count).count*pixarea) AS sum_part
			FROM 
				w_uhrn 
			-------------------			
			UNION ALL
			-------------------
			SELECT
				0.0 AS sum_part
			FROM
				w_ndsm_olil_katastr
			WHERE
				ident_null = false
			)
	---------------------------------------------------------------
	SELECT 
		sum(coalesce((sum_part),0)) AS aux_total_#CONFIG_ID#_#GID#
	FROM 
		w_sum;
	';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass_1 IS NULL)
	THEN
		reclass_text := 't2.#COLUMN_NAME_1#';
	ELSE
		reclass_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME_1#,#BAND_1#,ST_BandPixelType(t2.#COLUMN_NAME_1#,#BAND_1#),#RECLASS_VALUE_1#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS_1#', reclass_text);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#SCHEMA_NAME_1#', schema_name_1);
	result := replace(result, '#TABLE_NAME_1#', table_name_1);
	result := replace(result, '#COLUMN_NAME_1#', column_name_1);
	result := replace(result, '#BAND_1#', band_1::character varying);
	result := replace(result, '#CONDITION_1#', coalesce(condition_1::character varying, 'TRUE'));
	result := replace(result, '#UNIT_1#', unit_1::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass_1 IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE_1#', reclass_1::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové kategorie v rámci vektorové vrstvy.';

--------------------------------------------------------------------------------;

-- </function>
---------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------;
-- setting public privileges for sequences
--------------------------------------------------------------------------------;
GRANT ALL ON SEQUENCE @extschema@.cm_ext_gui_version_id_seq TO app_nfiesta_gisdata;
--------------------------------------------------------------------------------;