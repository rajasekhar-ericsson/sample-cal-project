--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app
(
	_gids4check	integer[],
	_gids		integer[]
)
RETURNS integer[] AS
$BODY$
DECLARE
	_cell_4check		integer[];
	_count_check		integer;
	_gids4next_step		integer[];
	_res_gids		integer[];
	_gids4check_original	integer[];
	_gids4check_except	integer[];
BEGIN
	-- pro vstupni _gids se zjisti jejich podrizene gidy
	SELECT array_agg(cmfac.cell)
	FROM @extschema@.cm_f_a_cell AS cmfac
	WHERE cmfac.cell_sup IN (SELECT unnest(_gids))
	INTO _cell_4check;

	WITH
	w1 AS	(SELECT unnest(_gids4check) AS gids4check),
	w2 AS	(SELECT unnest(_gids) AS gids)
	SELECT count(w1.*) FROM w1
	WHERE w1.gids4check IN (SELECT w2.gids FROM w2)
	INTO _count_check;

	-- pokud se se prichozi gidy z postupne prichozich estimation_cell
	-- stale nevyskytuji v seznamu gidu ZSJ, pak se tato funkce vola
	-- opakovane, stim, ze seznam prichozich gidu se rozsiruje o seznam
	-- gidu prodrizenych, tedy jde o spojeni vstupniho _gids a interniho
	-- seznamu _cell_4check

	-- pokud je promenna _count_check > 0, pak to znamena, ze hledani
	-- podrizenych gidu je ukonceno a do vystupu se vraci seznam gidu
	-- kdy se ze seznamu vstupnich _gids exceptem odstrani seznam gidu
	-- _gids4check

	-- vyjimka zde ale nastava u gidu [ZSJ], ktera sama osobne netvori
	-- celou jeji estimation_cell

	IF _count_check = 0
	THEN
		_gids4next_step := _gids || _cell_4check;
		_res_gids := @extschema@.fn_get_gids4under_estimation_cells_app(_gids4check,_gids4next_step);
	ELSE	
		_gids4check_original := _gids4check;
		_gids4check_except := _gids4check;
		
		-- pokud se v seznamu _gids4check vyskytuji gidy ZSJ, ktere sami o sobe
		-- netvori celou estimation_cell, se pak ze seznamu _gids4check musi odstranit,
		-- aby se tyto gidy ze seznamu _gids pak neodstranili

		FOR i IN 1..array_length(_gids4check,1)
		LOOP
			IF	(
				SELECT count(fac2.gid) > 1
				FROM @extschema@.f_a_cell AS fac2
				WHERE fac2.estimation_cell =
					(
					SELECT fac1.estimation_cell
					FROM @extschema@.f_a_cell AS fac1
					WHERE fac1.gid = _gids4check[i]
					)
				)
			THEN
				-- gid se ze seznamu _gids4check_original musi odstranit
				_gids4check_except := array_remove(_gids4check_except,_gids4check[i]);				
			ELSE
				-- gid se v seznamu _gids4check_original ponecha
				_gids4check_except := _gids4check_except;
			END IF;
		END LOOP;
	
		WITH
		w1 AS	(SELECT unnest(_gids || _gids4check_original) AS gids),
		w2 AS	(SELECT unnest(_gids4check_except) AS gids),
		w3 AS	(
			SELECT w1.gids FROM w1 except
			SELECT w2.gids FROM w2
			)
		SELECT array_agg(w3.gids) FROM w3
		INTO _res_gids;
	END IF;

	RETURN _res_gids;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) IS
'Funkce pro zadane gidy (druhy vstupni argument funkce _gids), ktere tvori estimation_cell, vraci gidy za vsechny podrizene estimation_cell. Vracene gidy jsou cizim klicem na pole gid do tabulky f_a_cell.';
