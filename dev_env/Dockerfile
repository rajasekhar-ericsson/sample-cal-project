FROM debian:buster
MAINTAINER Lukáš Kratěna <Kratena.Lukas@uhul.cz>

# fully upgrade system
RUN apt update && apt full-upgrade -y

# set locales to be able to use UTF-8
RUN apt install -y locales
RUN localedef -i cs_CZ -c -f UTF-8 -A /usr/share/locale/locale.alias cs_CZ.UTF-8
RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LC_MESSAGES=en_US.utf8 LC_COLLATE=cs_CZ.UTF-8 LC_CTYPE=cs_CZ.UTF-8 LC_MONETARY=cs_CZ.UTF-8 LC_NUMERIC=cs_CZ.UTF-8 LC_TIME=cs_CZ.UTF-8

# create your linux user (we will use user vagrant) and set password
RUN useradd --create-home --shell /bin/bash vagrant
RUN echo vagrant:vagrant | chpasswd

# set up sudo, add your user to sudoers group and allow to act as root and postgres user without asking password
RUN apt install -y sudo
RUN usermod -a -G sudo vagrant
RUN echo "vagrant ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user && echo "vagrant ALL=(postgres) NOPASSWD:ALL" >> /etc/sudoers.d/user && chmod 0440 /etc/sudoers.d/user

# install: make, gcc, git, wget, gnupg, ca-certificates
RUN apt update && apt install -y make gcc git wget gnupg ca-certificates

# add [pgdg](https://www.postgresql.org/download/linux/debian/) repository
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN wget --quiet --no-check-certificate https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN apt-key add ACCC4CF8.asc

# install latest postgres & postgis, corresponding postgresql-server-dev, plpython3 and python3-numpy
RUN apt update && apt install -y postgresql-12 postgresql-contrib-12 \
    postgresql-12-postgis-3 postgresql-12-postgis-3-scripts \
    postgresql-server-dev-12 \
    python3-psycopg2 \
    && apt-get clean

# Set the working directory to app home directory
WORKDIR /home/vagrant

# Specify the user to execute all commands below
USER vagrant
